package com.plexus.restapi.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.plexus.restapi.RestApiApplication;
import com.plexus.restapi.exception.RoleNotFoundException;
import com.plexus.restapi.exception.UserNotFoundException;
import com.plexus.restapi.model.Role;
import com.plexus.restapi.model.User;
import com.plexus.restapi.repository.UserRepository;
import com.plexus.restapi.services.TokenService;

/**
 * Representa a un usuario.
 * @author Gabriel
 *
 */
@RestController
@RequestMapping("/api")
public class UserController {
	
	final Logger logger = LoggerFactory.getLogger(RestApiApplication.class);
	
	private TokenService tokenService;
	
	UserController(TokenService tokenService) {
		this.tokenService = tokenService;
	}
	
	@Autowired
	UserRepository userRepository;
	
	/**
	 * Encripta la clave del usuario por medio de BCrypt Encoder.
	 * @param plainTextPassword La clave del usuario.
	 * @return Clave encriptada por la tecnología BCrypt Encoder.
	 */
	private String hashPassword(String plainTextPassword) {
		return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
	}
	
	/**
	 * Comprueba si coinciden las claves insertadas por el usuario.
	 * @param plainPassword La clave del usuario sin encriptar.
	 * @param hashedPassword La clave del usuario encriptada.
	 */
	private boolean checkPass(String plainPassword, String hashedPassword) {
		if (BCrypt.checkpw(plainPassword, hashedPassword)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Consigue todos los campos de los usuarios
	 * @return Un JSON de todos los usuarios
	 */
	@GetMapping("/users")
	public List<User> getUsers() {
		logger.debug("Funcion 'getUsers'. @GetMapping");
		
		return userRepository.findAll();
	}
	
	/**
	 * Crea un nuevo usuario a partir de la clase 'User'
	 * @param user Objeto de la clase 'User'
	 * @return Añade al JSON un nuevo objeto 'User'
	 */
	@PostMapping("/users")
	public User createUser(@RequestBody User user) {
		logger.debug("Funcion 'createUser'. @PostMapping");
		
		user.setPassword(hashPassword(user.getPassword()));
		
		return userRepository.save(user);
	}
	
	/**
	 * Comprueba el inicio de sesión.
	 * @param user El objeto usuario.
	 * @return	True si es correcta la validación.
	 * @throws SQLException
	 */
	@PostMapping("/login")
	public String checkLogin(@RequestBody User user) throws SQLException {
		String dbUrl = "jdbc:mysql://localhost:3306/restapi?useSSL=false";
		String query = "SELECT id_user, password_user FROM users WHERE name_user = '" + user.getName() + "'";
		
		Connection conn = DriverManager.getConnection(dbUrl, "root", "12345678");
		
		PreparedStatement stmt = conn.prepareStatement(query);
		
		ResultSet rs = stmt.executeQuery();
		
		if (rs.next()) {
			if (rs.getInt("id_user") > 0) {
				if (checkPass(user.getPassword(), rs.getString("password_user"))) {
					logger.debug("Se ha iniciado sesion satisfactoriamente.");
					return tokenService.createToken(user.getId());
				}
			}
		}
		logger.debug("Error al iniciar sesion.");
		return "null";
	}
	
	/**
	 * Consigue un usuario en concreto por medio del campo 'id'
	 * @param userId Valor del campo 'id'
	 * @return Devuelve los datos del usuario filtrado por su 'id'
	 */
	@GetMapping("/users/{id}")
	public Optional<User> getUserById(@PathVariable long id) {
		logger.debug("Funcion 'getUserById'. @GetMapping");
		
		Optional<User> userOptional = userRepository.findById(id);
		
		if (!userOptional.isPresent()) {
			throw new UserNotFoundException("El usuario con ID " + id + " no existe.");
		}
		
		return userOptional;
	}
	
	/**
	 * Actualiza los datos del usuario filtrado por el campo 'id'
	 * @param userId Valor del campo 'id'
	 * @param userDetails Valor de los demás campos del objeto 'User'
	 * @return Agrega al JSON los nuevos datos actualizados
	 */
	@PutMapping("/users/{id}")
	public ResponseEntity<Object> updateUser(@PathVariable long id, @Valid @RequestBody User user) {
		logger.debug("Función 'updateUser'. @PutMapping");
		
		Optional<User> userOptional = userRepository.findById(id);

		if (!userOptional.isPresent()) {
			throw new UserNotFoundException("El usuario con ID " + id + " no existe.");
		}

		user.setId(id);
		
		userRepository.save(user);

		return ResponseEntity.noContent().build();
	}
	
	/**
	 * Elimina un usuario filtrado por el campo 'id'.
	 * @param id Valor del campo 'id'.
	 * @return Elimina del JSON el usuario filtrado por el campo 'id'.
	 */
	@DeleteMapping("/users/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable long id) {
		logger.debug("Funcion 'deleteUser'. @DeleteMapping");
		
		User user = userRepository.findById(id).orElse(user = null);
		
		for(Role role: user.getRoles()) {
			role.getUsers().remove(user);
		}
		
		user.getRoles().clear();
		
		if (user == null) {
			throw new RoleNotFoundException("El usuario con ID " + id + " no existe.");
		}
		
		userRepository.deleteById(id);
		
		return ResponseEntity.ok().build();
	}

}
