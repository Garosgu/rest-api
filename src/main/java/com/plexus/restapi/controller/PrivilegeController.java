package com.plexus.restapi.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.plexus.restapi.RestApiApplication;
import com.plexus.restapi.exception.PrivilegeNotFoundException;
import com.plexus.restapi.model.Privilege;
import com.plexus.restapi.model.Role;
import com.plexus.restapi.repository.PrivilegeRepository;

/**
 * Representa un privilegio.
 * @author Gabriel
 *
 */
@RestController
@RequestMapping("/api")
public class PrivilegeController {
	
	final Logger logger = LoggerFactory.getLogger(RestApiApplication.class);
	
	@Autowired
	PrivilegeRepository privilegeRepository;
	
	/**
	 * Consigue todos los campos de los privilegios
	 * @return Un JSON de todos los privilegios
	 */
	@GetMapping("/privileges")
	public List<Privilege> getPrivileges() {
		logger.trace("Funcion 'getPrivileges'. @GetMapping");
		
		return privilegeRepository.findAll();
	}
	
	/**
	 * Crea un nuevo privilegio a partir de la clase 'Privilege'
	 * @param privilege Objeto de la clase 'Privilege'
	 * @return Añade al JSON un nuevo objeto 'Privilege'
	 */
	@PostMapping("/privileges")
	public Privilege createPrivilege(@Valid @RequestBody Privilege privilege) {
		logger.trace("Funcion 'createPrivilege'. @PostMapping");
		
		return privilegeRepository.save(privilege);
	}
	
	/**
	 * Consigue un privilegio en concreto por medio del campo 'id'
	 * @param privilegeId Valor del campo 'id'
	 * @return Devuelve los datos del privilegio filtrado por su 'id'
	 */
	@GetMapping("/privileges/{id}")
	public Optional<Privilege> getPrivilegeById(@PathVariable long id) {
		logger.trace("Funcion 'getPrivilegeById'. @GetMapping");
		
		Optional<Privilege> privilegeOptional = privilegeRepository.findById(id);
		
		if (!privilegeOptional.isPresent()) {
			throw new PrivilegeNotFoundException("El privilegio con ID " + id + " no existe.");
		}
		
		return privilegeOptional;
	}
	
	/**
	 * Actualiza los datos del privilegio filtrado por el campo 'id'
	 * @param privilegeId Valor del campo 'id'
	 * @param privilegeDetails Valor de los demás campos del objeto 'Privilege'
	 * @return Agrega al JSON los nuevos datos actualizados
	 */
	@PutMapping("/privileges/{id}")
	public ResponseEntity<Object> updatePrivilege(@PathVariable long id, @RequestBody Privilege privilege) {
		logger.trace("Funcion 'updatePrivilege'. @PutMapping");
		
		Optional<Privilege> privilegeOptional = privilegeRepository.findById(id);
		
		if (!privilegeOptional.isPresent()) {
			throw new PrivilegeNotFoundException("El privilegio con ID " + id + " no existe.");
		}
		
		privilege.setId(id);
		
		privilegeRepository.save(privilege);
		
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * Elimina un privilegio filtrado por el campo 'id'
	 * @param privilegeId Valor del campo 'id'
	 * @return Elimina del JSON el registro filtrado por el campo 'id'
	 */
	@DeleteMapping("/privileges/{id}")
	public ResponseEntity<Object> deletePrivilege(@PathVariable long id) {
		logger.trace("Funcion 'deletePrivilege'. @DeleteMapping");
		
		Privilege privilege = privilegeRepository.findById(id).orElse(privilege = null);
		
		for(Role role: privilege.getRoles()) {
			role.getPrivileges().remove(privilege);
		}
		
		if (privilege == null) {
			throw new PrivilegeNotFoundException("El privilegio con ID " + id + " no existe.");
		}
		
		privilegeRepository.deleteById(id);
		
		return ResponseEntity.ok().build();
	}

}
