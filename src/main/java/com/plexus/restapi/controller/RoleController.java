package com.plexus.restapi.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.plexus.restapi.RestApiApplication;
import com.plexus.restapi.exception.RoleNotFoundException;
import com.plexus.restapi.model.Role;
import com.plexus.restapi.model.User;
import com.plexus.restapi.repository.RoleRepository;

/**
 * Representa a un rol.
 * @author Gabriel
 *
 */
@RestController
@RequestMapping("/api")
public class RoleController {
	
	final Logger logger = LoggerFactory.getLogger(RestApiApplication.class);

	@Autowired
	RoleRepository roleRepository;
	
	/**
	 * Consigue todos los campos de los roles
	 * @return Un JSON de todos los roles
	 */
	@GetMapping("/roles")
	public List<Role> getRoles() {
		logger.trace("Funcion 'getRoles'. @GetMapping");
		
		return roleRepository.findAll();
	}
	
	/**
	 * Crea un nuevo rol a partir de la clase 'Role'
	 * @param role Objeto de la clase 'Role'
	 * @return Añade al JSON un nuevo objeto 'Role'
	 */
	@PostMapping("/roles")
	public Role createRole(@Valid @RequestBody Role role) {
		logger.trace("Funcion 'createRole'. @PostMapping");
		
		return roleRepository.save(role);
	}
	
	/**
	 * Consigue un rol en concreto por medio del campo 'id'
	 * @param roleId Valor del campo 'id'
	 * @return Devuelve los datos del rol filtrado por su 'id'
	 */
	@GetMapping("/roles/{id}")
	public Optional<Role> getRoleById(@PathVariable long id) {
		logger.trace("Funcion 'gerRoleById'. @GetMapping");
		
		Optional<Role> roleOptional = roleRepository.findById(id);
		
		if (!roleOptional.isPresent()) {
			throw new RoleNotFoundException("El rol con ID " + id + " no existe.");
		}
		
		return roleOptional;
	}
	
	/**
	 * Actualiza los datos del rol filtrado por el campo 'id'
	 * @param roleId Valor del campo 'id'
	 * @param roleDetails Valor de los demás campos del objeto 'Role'
	 * @return Agrega al JSON los nuevos datos actualizados
	 */
	@PutMapping("/roles/{id}")
	public ResponseEntity<Object> updateRole(@PathVariable long id, @RequestBody Role role) {
		logger.trace("Funcion 'updateRole'. @PutMapping");
		
		Optional<Role> roleOptional = roleRepository.findById(id);
		
		if (!roleOptional.isPresent()) {
			throw new RoleNotFoundException("El rol con ID " + id + " no existe.");
		}
		
		role.setId(id);
		
		roleRepository.save(role);
		
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * Elimina un rol filtrado por el campo 'id'
	 * @param roleId Valor del campo 'id'
	 * @return Elimina del JSON el registro filtrado por el campo 'id'
	 */
	@DeleteMapping("/roles/{id}")
	public ResponseEntity<Object> deleteRole(@PathVariable long id) {
		logger.trace("Funcion 'deleteRole'. @DeleteMapping");
		
		Role role = roleRepository.findById(id).orElse(role = null);
		
		for(User user: role.getUsers()) {
			user.getRoles().remove(role);
		}
		
		role.getPrivileges().clear();
		
		if (role == null) {
			throw new RoleNotFoundException("El rol con ID " + id + " no existe.");
		}
		
		roleRepository.deleteById(id);
		
		return ResponseEntity.ok().build();
	}
	
}
