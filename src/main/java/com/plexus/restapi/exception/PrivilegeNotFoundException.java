package com.plexus.restapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
public class PrivilegeNotFoundException extends RuntimeException {

	public PrivilegeNotFoundException(String exception) {
		super(exception);
	}
	
}
