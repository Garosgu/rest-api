package com.plexus.restapi.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "privileges")
public class Privilege {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_privilege;
	
	@Column(nullable = false)
	private String name_privilege;
	
	@ManyToMany(mappedBy = "privileges")
	@JsonIgnore
	private Set<Role> roles;
	
	public Privilege() { }
	
	/**
	 * Crea un privilegio con un nombre en específico.
	 * @param id ID del privilegio.
	 * @param name Nombre del privilegio.
	 */
	public Privilege(long id_privilege, String name_privilege) {
		this.id_privilege = id_privilege;
		this.name_privilege = name_privilege;
	}

	/**
	 * Consigue el ID del privilegio.
	 * @return ID que representa al privilegio.
	 */
	public long getId() {
		return id_privilege;
	}

	/**
	 * Establece el nuevo ID del privilegio.
	 * @param id El nuevo ID del privilegio.
	 */
	public void setId(long id) {
		this.id_privilege = id;
	}

	/**
	 * Consigue el nombre del privilegio.
	 * @return Nombre que representa al privilegio.
	 */
	public String getName() {
		return name_privilege;
	}

	/**
	 * Establece el nuevo nombre del privilegio.
	 * @param name El nuevo nombre del privilegio.
	 */
	public void setName(String name) {
		this.name_privilege = name;
	}

	/**
	 * Consigue el rol del privilegio.
	 * @return Rol que representa al privilegio.
	 */
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * Establece el nuevo rol del privilegio.
	 * @param roles El nuevo rol del privilegio.
	 */
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
}
