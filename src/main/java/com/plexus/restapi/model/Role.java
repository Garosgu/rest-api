package com.plexus.restapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.JoinColumn;

@Entity
@Table(name = "roles")
public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_role;
	
	@Column(nullable = false)
	private String name_role;
	
	@ManyToMany(mappedBy = "roles")
	@JsonIgnore
	private Set<User> users;

	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
			name = "privilege_roles",
			joinColumns = {@JoinColumn(name = "id_role")},
			inverseJoinColumns = {@JoinColumn(name = "id_privilege")}
	)
	private Set<Privilege> privileges;
	
	public Role() { }
	
	/**
	 * Crea un rol con un nombre en específico.
	 * @param id ID del rol.
	 * @param name Nombre del rol.
	 */
	public Role(long id_role, String name_role) {
		this.id_role = id_role;
		this.name_role = name_role;
	}

	/**
	 * Consigue el ID del rol.
	 * @return ID que representa al rol.
	 */
	public long getId() {
		return id_role;
	}

	/**
	 * Establece el nuevo ID del rol.
	 * @param id El nuevo ID del rol.
	 */
	public void setId(long id) {
		this.id_role = id;
	}

	/**
	 * Consigue el nombre del rol.
	 * @return Nombre que representa al rol.
	 */
	public String getName() {
		return name_role;
	}

	/**
	 * Establece el nuevo nombre del rol.
	 * @param name El nuevo nombre del rol.
	 */
	public void setName(String name) {
		this.name_role = name;
	}
	
	/**
	 * Consigue el usuario del rol
	 * @return Usuario que representa al rol.
	 */
	public Set<User> getUsers() {
		return users;
	}

	/**
	 * Establece el nuevo usuario del rol.
	 * @param users El nuevo usuario del rol.
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 * Consigue el privilegio del rol
	 * @return privilegio que representa al rol.
	 */
	public Set<Privilege> getPrivileges() {
		return privileges;
	}

	/**
	 * Establece el nuevo privilegio del rol.
	 * @param users El nuevo privilegio del rol.
	 */
	public void setPrivileges(Set<Privilege> privileges) {
		this.privileges = privileges;
	}

}
