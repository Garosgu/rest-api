package com.plexus.restapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_user;
	
	@Column(nullable = false)
	private String name_user;
	
	@Column
	private String password_user;
	
	@Column(nullable = false)
	private String email_user;
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
			name = "users_role",
			joinColumns = {@JoinColumn(name = "id_user")},
			inverseJoinColumns = {@JoinColumn(name = "id_role")}
	)
	private Set<Role> roles;
	
	public User() { }
	
	/**
	 * Crea un usuario con un nombre y email en específico.
	 * @param id ID del usuario.
	 * @param name Nombre del usuario.
	 * @param password Clave del usuario.
	 * @param email Email del usuario.
	 */
	public User(long id_user, String name_user, String password_user, String email_user) {
		this.id_user = id_user;
		this.name_user = name_user;
		this.password_user = password_user;
		this.email_user = email_user;
	}

	/**
	 * Consigue el ID del usuario.
	 * @return ID que representa al usuario.
	 */
	public long getId() {
		return id_user;
	}

	/**
	 * Establece el nuevo ID del usuario.
	 * @param id El nuevo ID del usuario.
	 */
	public void setId(long id) {
		this.id_user = id;
	}

	/**
	 * Consigue el nombre del usuario.
	 * @return Nombre que representa al usuario.
	 */
	public String getName() {
		return name_user;
	}

	/**
	 * Establece el nuevo nombre del usuario.
	 * @param name El nuevo nombre del usuario.
	 */
	public void setName(String name) {
		this.name_user = name;
	}

	/**
	 * Consigue la clave del usuario.
	 * @return Clave que representa al usuario.
	 */
	public String getPassword() {
		return password_user;
	}

	/**
	 * Establece la nueva clave del usuario
	 * @param password La nueva clave del usuario.
	 */
	public void setPassword(String password) {
		this.password_user = password;
	}

	/**
	 * Consigue el email del usuario.
	 * @return Email que representa al usuario.
	 */
	public String getEmail() {
		return email_user;
	}

	/**
	 * Establece el nuevo email del usuario.
	 * @param email El nuevo email del usuario.
	 */
	public void setEmail(String email) {
		this.email_user = email;
	}

	/**
	 * Consigue los roles del usuario.
	 * @return Rol que representa al usuario.
	 */
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * Establece el nuevo rol del usuario.
	 * @param roles El nuevo rol del usuario.
	 */
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

}
